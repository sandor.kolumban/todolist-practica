import { Box, Card, Grid, Typography } from '@material-ui/core';
import React from 'react';
import { ToDoItemProps } from '../Types/types';



export function ToDoItem(props:ToDoItemProps){
    return(
        <Card raised={true} key={props.itemId} color="primary" variant="outlined">
         <Box border={1} borderRadius={12} m={1} p={1} color="primary">
         <Grid container xs={12} color="primary">
            <Grid item xs={3}>
                <Typography variant="h5" component="h2">
                Description: 
                </Typography>
            </Grid>
            <Grid item xs={9}>
                {props.descriptions}
            </Grid>
            <Grid item xs={3}>
                State:
            </Grid>
            <Grid xs={3}>
                {props.state}
            </Grid>
            <Grid xs={3}>
            Created at:
            </Grid>
            <Grid xs={3}>
                {props.createdAt.toDateString()}
            </Grid>
        </Grid>
        </Box>
     </Card>
    );
}