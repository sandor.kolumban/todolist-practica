import React from "react";
import { ToDoItemProps } from "../Types/types";
import { ToDoItem } from "./ToDoItem";

export interface ToDoItemListProps{
    ToDoItems:ToDoItemProps[]
}

export class ToDoItemList extends React.Component<ToDoItemListProps> {

    constructor(props:ToDoItemListProps){
        super(props);
    }

    render(){
        return(
            this.props.ToDoItems.map(
                (itemProp:ToDoItemProps) =>
                <ToDoItem key={itemProp.itemId} itemId={itemProp.itemId} createdAt={itemProp.createdAt} descriptions={itemProp.descriptions} state={itemProp.state} />
                 )
            );
    }
}