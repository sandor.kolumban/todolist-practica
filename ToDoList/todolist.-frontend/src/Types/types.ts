export interface ToDoItemProps {
    key: string,
    itemId: string,
    state:ToDoState,
    descriptions:string,
    createdAt:Date
}

export enum ToDoState{
    NOT_STARTED = 'Not started',
    STARTED = 'Started',
    DONE = 'Done'
}