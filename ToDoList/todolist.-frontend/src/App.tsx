import React from 'react';
import './App.css';
import {FilteredToDoList} from './Componente/FilteredToDoList'
import { ToDoState } from './Types/types';

const ToDoList = [
  {
    key:"2",
    itemId:"2",
    createdAt:new Date(2021,12,5),
    descriptions:"Text 1 important",
    state:ToDoState.NOT_STARTED
  },
  {
    key:"3",
    itemId:"3",
    createdAt:new Date(2021,12,5),
    descriptions:"Text 2 important",
    state:ToDoState.NOT_STARTED
  },
  {
    key:"4",
    itemId:"4",
    createdAt:new Date(2021,12,5),
    descriptions:"Text 3 important",
    state:ToDoState.NOT_STARTED
  },
  {
    key:"5",
    itemId:"5",
    createdAt:new Date(2021,12,5),
    descriptions:"Text 4 important",
    state:ToDoState.NOT_STARTED
  },
  {
    key:"6",
    itemId:"6",
    createdAt:new Date(2021,12,5),
    descriptions:"Text 5 important",
    state:ToDoState.NOT_STARTED
  },
]

function App() {
  return (
    <div className="App">
      <FilteredToDoList ToDoList={ToDoList}/>
    </div>
  );
}

export default App;
